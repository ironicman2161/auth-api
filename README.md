# Autenticação de usuário com a api 

App desenvolvido para testes de comunicação com o axios
Foi desenvolvida a logica de armazenamento de token para manter o usuario logado ou deslogado do app

## Desenvolvimento

## Requerimentos
- `Expo`
- `NPM`

### Instalação

- `npm install` ou `yarn install`

## Iniciar Execução
- `Alterar a BaseURL para seu ip/nome da api`
- `expo start`
