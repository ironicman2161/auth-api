import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const TOKEN_KEY = "@Teste:token";

export const onSignOut = async () => {
  try {
    await AsyncStorage.removeItem(TOKEN_KEY)
  } catch (e) {
    console.log(e);
  }
}
export const onSignIn = async (response) => {
  console.log(response)
  try {
    await AsyncStorage.setItem(TOKEN_KEY, "true")
  } catch (e) {
    console.log(e);
  }
}

export const isSignedIn = async () => {
  try {
    const token = await AsyncStorage.getItem(TOKEN_KEY)
    return (token !== null) ? true : false;
  } catch (e) {
    console.log(e);
  }
};