import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import Login from './pages/login';
import Logged from './pages/logged';

const LoggedStack = createStackNavigator();
function SignedOutRoutes() {
  return (
    <LoggedStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#644b6e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#cf9ee3'
        }
      }}>
      <LoggedStack.Screen name="Meu perfil" component={Logged} options={{
        title: 'Meu perfil', headerTitleAlign: 'right',
      }} />
    </LoggedStack.Navigator>
  );
}
const LoginStack = createStackNavigator();
function SignedInRoutes() {
  return (
    <LoginStack.Navigator>
      <LoginStack.Screen name="Entrar" component={Login} options={{
        title: 'Entrar', headerTitleAlign: 'right',
      }} />
    </LoginStack.Navigator>
  );
}
const Tab = createStackNavigator();
export function CreateRootNavigator(signedIn = false) {
  console.log(signedIn ? "SignedOut" : "SignedIn");
  return (
    <NavigationContainer>
      <Tab.Navigator initialRouteName={signedIn ? "SignedOut" : "SignedIn"}>
        <Tab.Screen name="SignedIn" component={SignedInRoutes} />
        <Tab.Screen name="SignedOut" component={SignedOutRoutes} />
      </Tab.Navigator>
    </NavigationContainer>
  )
}



/*export const SignedOutRoutes = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      title: "Entrar"
    }
  },
});
export const SignedInRoutes = createStackNavigator({
  Logged: {
    screen: Logged,
    navigationOptions: {
      title: "Meu perfil"
    }
  },
});*/
/*export const createRootNavigator = (signedIn = false) => {
  return createStackNavigator({
    SignedIn: { screen: SignedInRoutes },
    SignedOut: { screen: SignedOutRoutes }
  },
    {
      headerMode: "none",
      mode: "modal",
      initialRouteName: signedIn ? "SignedIn" : "SignedOut",
      navigationOptions: {
        gesturesEnabled: false
      }
    });
}*/