import React from 'react';
import { View, Text, Input, TouchableOpacity } from 'react-native';
import { onSignIn } from '../auth';
import api from '../axios'

export default function login(props) {
  async function sendData() {
    await api.post('login', {
      email: 'sample@email.com',
      password: 'password'
    })
    .then(response => onSignIn(response.data).then(() => props.navigation.replace('SignedOut')))
    .catch(err => console.log(err.request.response));
    
  }
  return (
    <View style={{ paddingTop: 150 }}>
      <View style={{ alignItems: 'center' }}>
        <TouchableOpacity
          style={{ padding: 10, backgroundColor: "#03A9F4" }}
          onPress={() => {
            sendData();
          }}
        ><Text style={{ fontSize: 30 }}>Entrar</Text></TouchableOpacity>
      </View>
    </View>
  )
};