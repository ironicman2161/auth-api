import api from '../axios'
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { onSignOut, App } from '../auth';

export default (props) => (
  <View style={{ paddingVertical: 20 }}>
    <View style={{ alignItems: "center" }}>
      <View
        style={{
          backgroundColor: "#bcbec1",
          alignItems: "center",
          justifyContent: "center",
          width: 80,
          height: 80,
          borderRadius: 40,
          alignSelf: "center",
          marginBottom: 20
        }}
      >
        <Text style={{ color: "white", fontSize: 28 }}>JD</Text>
      </View>
      <TouchableOpacity
        style={{ marginTop: 20, backgroundColor: "#dd3333", padding: 10 }}
        onPress={() => {
          onSignOut().then(() => props.navigation.replace('SignedIn'))
        }}
      ><Text style={{ fontSize: 25, color: '#fff' }}>Sair</Text></TouchableOpacity>
    </View>
  </View>
);